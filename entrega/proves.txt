reiEscapa = Tauler [(DB,Posicio('e',7)),(AB,Posicio('a',6)),(RN,Posicio('c',6)),(TB,Posicio('e',6)),(TB,Posicio('c',4)),(RB,Posicio('e',2))]
reiNoEscapa = Tauler [(DB,Posicio('e',7)),
                      (AB,Posicio('a',6)),(RN,Posicio('c',6)),(TB,Posicio('e',6)),
					  (AB,Posicio('e',5)),(TB,Posicio('c',4)),(RB,Posicio('d',4))]
reiEsSalvat = Tauler [(DB,Posicio('e',8)),
                      (AN,Posicio('c',7)),(PN,Posicio('d',7)),(AN,Posicio('f',7)),
					  (AB,Posicio('a',6)),(RN,Posicio('c',6)),(TB,Posicio('e',6)),
					  (AB,Posicio('e',5)),(PN,Posicio('h',5)),(RB,Posicio('d',4))]
reiEsSalvat2 = Tauler [(DB,Posicio('e',8)),
                       (AN,Posicio('c',7)),(PN,Posicio('d',7)),(AB,Posicio('a',6)),(RN,Posicio('c',6)),(AN,Posicio('e',6)),(AB,Posicio('e',5)),(PN,Posicio('h',5)),(RB,Posicio('d',4))]


Inserir existent

putStrLn(mostrarPeces(Tauler(inserirOrdenat (PN, Posicio('c',8)) [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('e',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('e',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(DB,Posicio('d',1)),(RB,Posicio('e',1)),(AB,Posicio('f',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))])))
						
Inserir nou

putStrLn(mostrarPeces(Tauler(inserirOrdenat (PN, Posicio('c',4)) [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('e',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('e',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(DB,Posicio('d',1)),(RB,Posicio('e',1)),(AB,Posicio('f',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))])))

Eliminar existent
					
putStrLn(mostrarPeces(Tauler(eliminarPosicio (Posicio('c',8)) [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('e',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('e',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(DB,Posicio('d',1)),(RB,Posicio('e',1)),(AB,Posicio('f',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))])))

Eliminar buit						
putStrLn(mostrarPeces(Tauler(eliminarPosicio (Posicio('c',5)) [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('e',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('e',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(DB,Posicio('d',1)),(RB,Posicio('e',1)),(AB,Posicio('f',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))])))
						
Combinacio 

putStrLn(mostrarPeces(Tauler(inserirOrdenat (PN, Posicio('c',4)) (eliminarPosicio (Posicio('c',8)) [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('e',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('e',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(DB,Posicio('d',1)),(RB,Posicio('e',1)),(AB,Posicio('f',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]))))
						
Fer jugada
	fesJugada taulerInicial (PN, Posicio('c',4), Posicio('c',8),"")
	
	
Moviments possibles 

movimentsPossibles (fesJugada taulerInicial (PN, Posicio('e',2), Posicio('e',4),"")) (RB,Posicio('e',1))


PotEscapar 
TAULER pastor ultim
Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]

fesJugada (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (DB, Posicio('h',5), Posicio('f',7),"")

NO pot escapar
potEscapar (fesJugada ( Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (DB, Posicio('h',5), Posicio('f',7),"")) (RN,Posicio('e',8))
						
SI pot escapar
potEscapar (fesJugada (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(RN,Posicio('d',8)),(AN,Posicio('f',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (DB, Posicio('h',5), Posicio('f',7),"")) (RN,Posicio('e',8))
						
movimentsPossibles (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (RN,Posicio('e',8))
						
escac (fesJugada (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (RN,Posicio('e',8),Posicio('f',8),"")) (colorPeca RN)
				
potEscapar (fesJugada (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (DB, Posicio('h',5), Posicio('f',7),"")) (RN,Posicio('e',8))
						
						
						
esPotProtegir
esPotProtegir (fesJugada (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(RN,Posicio('d',8)),(AN,Posicio('f',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (DB, Posicio('h',4), Posicio('f',6),"")) (RN,Posicio('d',8))



hauria de protegir i no protegeix; arreglat, l'alfil protegeix
esPotProtegir (fesJugada (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(RN,Posicio('d',8)),(AN,Posicio('f',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),(DB,Posicio('h',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (DB, Posicio('h',5), Posicio('e',7),"")) (RN,Posicio('d',8))

esPotProtegir amb peo fictici						
esPotProtegir (fesJugada (Tauler[(TN,Posicio('a',8)),(AN,Posicio('c',8)),(PN,Posicio('d',8)),(RN,Posicio('f',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)), (DB,Posicio('e',7)), (PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(CN,Posicio('c',6)),(CN,Posicio('f',6)),
						(PN,Posicio('e',5)),
						(AB,Posicio('c',4)),(PB,Posicio('e',4)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(RB,Posicio('e',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]) (DB, Posicio('h',5), Posicio('e',7),"")) (RN,Posicio('',8))
						
						
totesPosicioPecaFaEscac pastorUltim RN (Posicio('e',8)) [(Posicio('e',7))]

faEscac :: Tauler -> (Peca,Posicio) -> Posicio -> Bool

taulerInicial = Tauler [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(RN,Posicio('e',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(AB,Posicio('d',6)),
						(PN,Posicio('b',5)),(CB,Posicio('d',5)),(PB,Posicio('e',5)),(CB,Posicio('f',5)),(PB,Posicio('h',5)),
						(PB,Posicio('g',4)),
						(PB,Posicio('d',3)),(DB,Posicio('f',3)),
                        (PB,Posicio('a',2)),(DN,Posicio('b',2)),(PB,Posicio('c',2)),
                        (TB,Posicio('a',1)),(RB,Posicio('f',1)),(AN,Posicio('g',1))]

Tauler [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(RN,Posicio('e',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(AB,Posicio('d',6)),
						(PN,Posicio('b',5)),(CB,Posicio('d',5)),(PB,Posicio('e',5)),(CB,Posicio('f',5)),(PB,Posicio('h',5)),
						(PB,Posicio('g',4)),
						(PB,Posicio('d',3)),(DB,Posicio('f',3)),
                        (PB,Posicio('a',2)),(PB,Posicio('c',2)),
                        (DN,Posicio('a',1)),(RB,Posicio('f',1)),(AN,Posicio('g',1))]


((R,('f',1)),[('f',2),('g',1),('e',1),('g',2),('e',2)])

nouTauler333 = Tauler [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(RN,Posicio('e',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('d',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
						(AB,Posicio('d',6)),
						(PN,Posicio('b',5)),(CB,Posicio('d',5)),(PB,Posicio('e',5)),(CB,Posicio('f',5)),(PB,Posicio('h',5)),
						(PB,Posicio('g',4)),
						(PB,Posicio('d',3)),(DB,Posicio('f',3)),
                        (PB,Posicio('a',2)),(PB,Posicio('c',2)),(RB,Posicio('e',2)),
                        (DN,Posicio('a',1)),(AN,Posicio('g',1))]
